import "./App.css";
import ProductPage from "./pages/main-product-page/main-product-page";
import ProductDetailPage from "./pages/product-detail-page/product-detail-page";
import ToolbarComponent from "./components/toolbar/toolbar";
import { Routes, Route } from "react-router-dom";
import { useEffect } from "react";

function App() {
  useEffect(() => {
    // Update the document title using the browser API
  });
  return (
    <div>
      <ToolbarComponent></ToolbarComponent>
      <div className="page-container">
        <Routes>
          <Route path="/" element={<ProductPage />} />
          <Route path="/detail/:id" element={<ProductDetailPage />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
