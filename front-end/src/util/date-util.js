export function getTimeRemaining(endDate) {
  const total = endDate - Date.parse(new Date());
  const seconds = Math.floor((total / 1000) % 60);
  const minutes = Math.floor((total / 1000 / 60) % 60);
  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
  const days = Math.floor(total / (1000 * 60 * 60 * 24));
  // if(hours<0 || hours<0 || hours<0 || hours<0 || hours<0 || hours<0){

  // }
  if (total < 0) {
    return undefined;
  }
  return {
    total,
    seconds,
    minutes,
    hours,
    days,
  };
}
