import { GET_PRODUCT } from "../types";
import axios from "axios";

export const getProduct = () => async (dispatch) => {
  try {
    const res = await axios.get(`http://localhost:4100/init`);

    dispatch({
      type: GET_PRODUCT,
      payload: res.data.lst,
    });
  } catch (e) {
    // dispatch( {
    //     type: USERS_ERROR,
    //     payload: console.log(e),
    // })
  }
};
