import { AUTH_SUCESS, AUTH_FAIL, IS_AUTH } from "../types";
import axios from "axios";

export const authentication = (username, password) => async (dispatch) => {
  try {
    const res = await axios.post(`http://localhost:4100/login`, {
      username,
      password,
    });
    sessionStorage.setItem("AUTH", res.data);
    dispatch({
      type: AUTH_SUCESS,
      payload: res.data,
    });
  } catch (e) {
    dispatch({
      type: AUTH_FAIL,
    });
  }
};
export const signOut = () => (dispatch) => {
  sessionStorage.removeItem("AUTH");
  dispatch({
    type: AUTH_FAIL,
  });
};

export const checkAuthorize = () => (dispatch) => {
  const auth = sessionStorage.getItem("AUTH");
  if (auth) {
    dispatch({
      type: AUTH_SUCESS,
      payload: auth,
    });
  } else {
    dispatch({
      type: AUTH_FAIL,
    });
  }
};
// export const i
// export const isAuth = async (dispatch) => {
//   dispatch({
//     type: IS_AUTH,
//   });
// };
