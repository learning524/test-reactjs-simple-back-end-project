import { AUTH_SUCESS, AUTH_FAIL } from "../types";

const initialState = {
  isAuth: false,
  userName: "",
  loading: false,
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
  switch (action.type) {
    case AUTH_SUCESS:
      return {
        ...state,
        isAuth: true,
        username: action.payload.username,

        loading: false,
      };
    case AUTH_FAIL:
      return {
        ...state,
        isAuth: false,
        userName: "",
        loading: false,
      };
    default:
      return state;
  }
}
