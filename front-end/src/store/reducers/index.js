import { combineReducers } from "redux";
import productReducer from "./product-reducer.js";
import authReducer from "./auth-reducer";

export default combineReducers({
  products: productReducer,
  authen: authReducer,
});
