import { GET_PRODUCT } from "../types";

const initialState = {
  lst: [],
  loading: true,
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCT:
      return {
        ...state,
        lst: action.payload,
        loading: false,
      };
    default:
      return state;
  }
}
