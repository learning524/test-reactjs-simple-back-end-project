import "./toolbar.css";
import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";

import TextField from "@mui/material/TextField";
import {
  authentication,
  signOut,
  checkAuthorize,
} from "../../store/actions/auth-action";
import { connect } from "react-redux";
import { styled } from "@mui/material/styles";
import Popover from "@mui/material/Popover";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleUser, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";

const CssTextField = styled(TextField, {
  shouldForwardProp: (props) => props !== "focusColor",
})((p) => ({
  // input label when focused
  "& label.Mui-focused": {
    color: "#12284c",
  },
  // focused color for input with variant='standard'
  "& .MuiInput-underline:after": {
    borderBottomColor: "#12284c",
  },
  // focused color for input with variant='filled'
  "& .MuiFilledInput-underline:after": {
    borderBottomColor: "#12284c",
  },
  // focused color for input with variant='outlined'
  "& .MuiOutlinedInput-root": {
    "&.Mui-focused fieldset": {
      borderColor: "#12284c",
    },
  },
}));

class ToolbarComponent extends React.Component {
  constructor(props) {
    super();

    this.state = {
      username: "",
      password: "",
      anchorEl: null,
      isMobile: this.isMobile(),
      isProductDetailPath: this.isProductDetailPath(),
    };
  }
  componentDidMount() {
    this.props.checkAuthorize();
    window.addEventListener(
      "resize",
      () => {
        this.setState({
          isMobile: this.isMobile(),
        });
      },
      false
    );
    // this.unlisten = browserHistory.listen((location) => {
    // this.setState({ isProductDetailPath: this.isProductDetailPath() });
    // });
  }
  componentDidUpdate(prvProps) {
    if (prvProps.location.pathname !== this.props.location.pathname) {
      this.setState({ isProductDetailPath: this.isProductDetailPath() });
    }
  }
  isMobile() {
    return window.innerWidth < 959;
  }
  isProductDetailPath() {
    return window.location.href.indexOf("http://localhost:3000/detail") !== -1;
  }

  handleClick = (event) => {
    // setAnchorEl(event.currentTarget);
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  handleValueChange(e, propKey) {
    this.setState({ [propKey]: e.target.value });
  }
  singIn() {
    if (!this.state.username || !this.state.password) {
      return false;
    }
    this.props.authentication(this.state.username, this.state.password);
  }
  signOut() {
    this.props.signOut();
  }
  getWebControlBtn() {
    // console.log(this.props.authen);
    const open = Boolean(this.state.anchorEl);
    const id = open ? "simple-popover" : undefined;
    if (!this.props.authen.isAuth) {
      return (
        <div className="username-pass-container">
          <CssTextField
            value={this.state.username}
            color="secondary"
            InputProps={{
              style: {
                height: "40px",
              },
            }}
            onChange={(e) => this.handleValueChange(e, "username")}
            label="username"
          />

          <CssTextField
            color="secondary"
            label="password"
            value={this.state.password}
            InputProps={{
              style: {
                height: "40px",
              },
            }}
            type="password"
            onChange={(e) => this.handleValueChange(e, "password")}
          />

          <Button
            // color="primary"
            className="btn-toolbar"
            variant="contained"
            onClick={() => this.singIn()}
          >
            Sign in
          </Button>
        </div>
      );
    } else {
      return (
        <div>
          <IconButton
            className="user-icon-btn"
            aria-describedby={id}
            onClick={this.handleClick}
          >
            <FontAwesomeIcon icon={faCircleUser}></FontAwesomeIcon>
          </IconButton>

          <Popover
            id={id}
            open={open}
            anchorEl={this.state.anchorEl}
            onClose={this.handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
          >
            <div className="user-info-box">
              <div className="user-info">
                <div className="user-info-icon"></div>

                <div className="user-info-username">
                  <div>username: {this.props.authen.username}</div>
                </div>
              </div>
              <Button
                // color="primary"
                className="btn-toolbar"
                variant="contained"
                onClick={() => this.signOut()}
              >
                Sign out
              </Button>
            </div>
          </Popover>
        </div>
      );
    }
  }
  getProductdetailMobileControlBtn() {
    return (
      <div className="header-product-detail-moblie">
        <Link to={"/"}>
          <IconButton className="btn-back">
            <FontAwesomeIcon icon={faChevronLeft}></FontAwesomeIcon>
          </IconButton>
        </Link>
        <span className="title primary-text">ประมูลสินค้า</span>
      </div>
    );
  }
  render() {
    return (
      <Box sx={{ flexGrow: 1 }} className="toolbar-box">
        <AppBar position="static">
          <Toolbar className="main-tool-bar">
            {this.state?.isMobile && this.state?.isProductDetailPath ? (
              ""
            ) : (
              <div className="logo">
                <span>LOGO</span>
              </div>
            )}
            <div className="auth-input">
              {this.state?.isMobile && this.state?.isProductDetailPath
                ? this.getProductdetailMobileControlBtn()
                : this.getWebControlBtn()}
            </div>
          </Toolbar>
        </AppBar>
      </Box>
    );
  }
}

// export default ToolbarComponent;

const mapStateToProps = (state) => {
  return { authen: state.authen };
};

const ToolbarComp = (props) => (
  <ToolbarComponent {...props} location={useLocation()} />
);
export default connect(mapStateToProps, {
  authentication,
  signOut,
  checkAuthorize,
})(ToolbarComp);
