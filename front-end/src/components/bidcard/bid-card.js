import "./bid-card.css";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import AddCircleOutlineOutlinedIcon from "@mui/icons-material/AddCircleOutlineOutlined";
import { getTimeRemaining } from "../../util/date-util";
import NumberFormat from "react-number-format";
import Moment from "moment";
import "moment/locale/th";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import axios from "axios";
import { connect } from "react-redux";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
class BidCardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.productId = props.product_id;
    this.title = props.title;
    this.price = props.price;
    this.bidPrice = this.price;
    this.endDate = new Date(parseInt(props.endDate));
    this.dateRemaining = getTimeRemaining(this.endDate);
    this.bid = props.bid;

    this.state = {
      bidPrice: this.price,
      open: false,
      alertMsg: "",
      severity: "error",
      price: this.price,
    };
  }
  componentDidUpdate(prevProps) {
    if (this.props.price !== prevProps.price) {
      this.price = this.props.price;
      this.setState({ price: this.price });
    }
  }

  bidAddClicked() {
    this.bidPrice = this.bidPrice + this.bid;
    this.setState({ bidPrice: this.bidPrice });
  }
  async bidClick() {
    if (!this.props.authen.isAuth) {
      this.setState({
        alertMsg: "Please Login Before Proceed",
        severity: "error",
        open: true,
      });
      return;
    }
    try {
      await axios.post(`http://localhost:4100/updateBid`, {
        product_id: parseInt(this.productId),
        updateUser: 0,
        bid: parseInt(this.state.bidPrice),
      });
      this.setState({
        alertMsg: "Bid Success",
        severity: "success",
        open: true,
      });
    } catch (e) {
      if (e.response.status === 406) {
        this.setState({
          alertMsg: e.response.data.msg,
          severity: "error",
          open: true,
        });
      } else {
        this.setState({
          alertMsg: "Please try to contact our staff",
          severity: "error",
          open: true,
        });
      }
    }
  }
  snackBarClose(reason) {
    this.setState({ open: false });
  }

  render() {
    // Moment.locale("th");
    Moment.locale("th");

    return (
      <div className="bid-container">
        <div className="image"></div>
        <div className="bid-detail-container">
          <div className="product-detail">
            <div className="product-name primary-text">{this.title}</div>
            <div className="bid-detail">
              <span className="detail  light-th-body2 secondary-text">
                ประมูลขั้นต่ำ ครั้งละ{" "}
                <NumberFormat
                  value={this.bid}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"฿"}
                ></NumberFormat>
              </span>
              <span className="detail light-th-body2 secondary-text">|</span>
              <span className="detail light-th-body2 secondary-text">
                ปิดประมูล{" "}
                {Moment(this.endDate)
                  .add(543, "year")
                  .format("DD MMM YY hh:mm") + "น."}
              </span>
            </div>
          </div>
          <div className="price-time-container">
            {this.dateRemaining ? (
              <div className="time-expire">
                <span className="title secondary-text">จะหมดเวลาในอีก</span>
                <div className="time-container">
                  <span className="icon">
                    <FontAwesomeIcon
                      icon={faClock}
                      className="secondary-text"
                    />
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    {this.dateRemaining.days} วัน
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    :
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    {this.dateRemaining.hours}
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    :
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    {this.dateRemaining.minutes}
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    :
                  </span>
                  <span className="time semi-bold-en-subtilte primary-text ">
                    {this.dateRemaining.seconds}
                  </span>
                </div>
              </div>
            ) : (
              <div className="time-expire">
                <span className="title secondary-text">หมดเวลาในการประมูล</span>
              </div>
            )}

            <div className="price-container">
              <div className="title-container">
                <span className="title light-th-body secondary-text ">
                  ราคาประมูลปัจจุบัน
                </span>
                <span className="price secondary-text semi-bold-th-body">
                  {" "}
                  <NumberFormat
                    value={this.state.price}
                    displayType={"text"}
                    thousandSeparator={true}
                    prefix={"฿"}
                  ></NumberFormat>
                </span>
              </div>
              <div className="input-container">
                <NumberFormat
                  value={this.state.bidPrice}
                  className="bid-input"
                  thousandSeparator={true}
                  customInput={TextField}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start" className="input-prefix">
                        ฿
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position="end" className="input-suffix">
                        <IconButton
                          aria-label="toggle password visibility"
                          edge="end"
                          className="icon "
                          onClick={() => this.bidAddClicked()}
                        >
                          <AddCircleOutlineOutlinedIcon></AddCircleOutlineOutlinedIcon>
                        </IconButton>
                        <span className="bid primary-text semi-bold-th-body">
                          <NumberFormat
                            value={this.bid}
                            displayType={"text"}
                            thousandSeparator={true}
                            customInput={TextField}
                          ></NumberFormat>
                        </span>
                      </InputAdornment>
                    ),
                    style: {
                      height: "40px",
                      width: "100%",
                    },
                    readOnly: true,
                  }}
                ></NumberFormat>
                <Button
                  variant="contained"
                  className="btn-bid-auction"
                  onClick={() => this.bidClick()}
                >
                  เสนอราคา
                </Button>
              </div>
            </div>
          </div>
        </div>
        <Snackbar
          open={this.state.open}
          autoHideDuration={2000}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
          onClose={(event, reason) => this.snackBarClose(reason)}
        >
          <Alert
            severity={this.state.severity}
            sx={{ width: "100%" }}
            iconMapping={{
              error: <div className="alert-error-icon"></div>,
            }}
          >
            {this.state?.alertMsg}
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

// export default BidCardComponent;
const mapStateToProps = (state) => {
  return { authen: state.authen };
};

export default connect(mapStateToProps, {})(BidCardComponent);
