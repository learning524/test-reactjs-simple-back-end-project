import "./bid-table.css";
import React from "react";
// import * as React from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faClock } from "@fortawesome/free-solid-svg-icons";
import { faUserTag } from "@fortawesome/free-solid-svg-icons";
import NumberFormat from "react-number-format";
import Moment from "moment";
import "moment/locale/th";
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.MuiTableCell-root`]: {
    fontFamily: `"Metropolis", "Noto Sans Thai"`,
    fontStyle: "normal",
    fontWeight: "600",
    fontSize: "16px",
    lineHeight: "26px",
    color: "#393F49",
  },
}));

class BidTableComponet extends React.Component {
  constructor(props) {
    super();
    // this.bidHis= props.his
    this.state = {
      bidHis: props.hisData,
    };
  }
  componentDidUpdate(prevProps) {
    if (this.props.hisData.length !== prevProps.hisData.length) {
      this.setState({ bidHis: this.props.hisData });
    }
  }
  getRowClass(index) {
    if (index === 0) {
      return "first-row";
    } else if (index === this.state.bidHis.length - 1) {
      return "lastRow";
    } else {
      return "";
    }
  }
  render() {
    Moment.locale("th");
    return (
      <div className="bid-table-container ">
        <TableContainer component={Paper} elevation={0}>
          <Table className="bid-table-his primary-text">
            <TableHead>
              <TableRow>
                <StyledTableCell className="header first-header">
                  ราคา
                </StyledTableCell>
                <StyledTableCell className="header">
                  <FontAwesomeIcon icon={faUserTag}></FontAwesomeIcon> ผู้ประมูล
                </StyledTableCell>

                <StyledTableCell className="header last-header col-bid-date">
                  เวลาที่ประมูล
                </StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.bidHis.map((row, index) => (
                <TableRow
                  key={row.bid + "-" + row.updateDate}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  className={this.getRowClass(index)}
                >
                  <StyledTableCell>
                    <NumberFormat
                      value={row.bid}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"฿"}
                    ></NumberFormat>
                  </StyledTableCell>
                  <StyledTableCell>
                    <div className="col-user-name-content">
                      <span> {row.user.username}</span>
                      <span className="update-by light-th-body2 ">
                        {" "}
                        {Moment(row.updateDate)
                          .add(543, "year")
                          .format("DD MMM YY hh:mm") + "น."}
                      </span>
                    </div>
                  </StyledTableCell>
                  <StyledTableCell className="col-bid-date">
                    {Moment(row.updateDate)
                      .add(543, "year")
                      .format("DD MMM YY hh:mm") + "น."}
                  </StyledTableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}

export default BidTableComponet;
