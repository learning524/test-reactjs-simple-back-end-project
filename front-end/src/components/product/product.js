import "./product.css";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import NumberFormat from "react-number-format";
import { getTimeRemaining } from "../../util/date-util";
class ProductComponent extends React.Component {
  constructor(props) {
    super();
    this.id = props.id;
    this.title = props.title;
    this.endDate = new Date(parseInt(props.endDate));
    this.dateRemaining = getTimeRemaining(this.endDate);
    this.price = props.price;
    this.onClickProduct = this.onClickProduct.bind(this);
  }
  onClickProduct() {}

  render() {
    return (
      <div className="product-container">
        <div className="img"></div>

        <div className="detail-container">
          <div className="product-detail">
            <Link
              to={"/detail/" + this.id}
              className="product-name primary-text"
            >
              <div>{this.title}</div>
            </Link>
            <div className="product-time">
              <span className="icon primary-text">
                <FontAwesomeIcon icon={faClock} />
              </span>

              <span className="time semi-bold-th-body2 primary-text">
                {this.dateRemaining.days} วัน
              </span>
              <span className="time semi-bold-th-body2 primary-text">:</span>
              <span className="time semi-bold-th-body2 primary-text">
                {this.dateRemaining.hours}
              </span>
              <span className="time semi-bold-th-body2 primary-text">:</span>
              <span className="time semi-bold-th-body2 primary-text">
                {this.dateRemaining.minutes}
              </span>
              <span className="time semi-bold-th-body2 primary-text">:</span>
              <span className="time semi-bold-th-body2 primary-text">
                {this.dateRemaining.seconds}
              </span>
            </div>
          </div>
          <div className="price-container">
            <span className="price">
              <NumberFormat
                value={this.price}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"฿"}
              ></NumberFormat>
            </span>

            {/* <Link to="/about">
              <Button variant="contained" className="btn-auction">
                ประมูล
              </Button>
            </Link> */}
            <Link to={"/detail/" + this.id}>
              <Button variant="contained" className="btn-auction">
                ประมูล
              </Button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductComponent;
