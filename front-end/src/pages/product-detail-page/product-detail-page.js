import React from "react";
import "./product-detail-page.css";
import { useParams } from "react-router-dom";
import BidCardComponent from "../../components/bidcard/bid-card";
import BidTableComponet from "../../components/bid-table/bid-table";
import axios from "axios";
class ProductDetailPage extends React.Component {
  constructor(props) {
    super();
    this.id = props.params.id;

    this.state = {
      product_title: "",
      current_price: "",
      end_date: "",
      bid_price: "",
      bidHis: [],
    };
  }
  componentDidMount() {
    this.refreshTable();
    this.tableInterval = setInterval(() => this.refreshTable(), 3000);
    // this.productDetail = res.data;
  }

  async refreshTable() {
    const res = await axios.get(
      `http://localhost:4100/productdetail?prodid=${this.id}`
    );
    // console.log({ ...this.state.productDetail, ...res.data });
    this.setState({
      product_title: res.data.product_title,
      current_price: res.data.current_price,
      end_date: res.data.end_date,
      bid_price: res.data.bid_price,
      bidHis: res.data.bidHis,
    });
  }
  componentWillUnmount() {
    clearInterval(this.tableInterval);
  }
  render() {
    return (
      <div>
        <div className="title-container">
          <span className="light-th-body2 primary-text"> ประมูล</span>
          <span className="light-th-body2 primary-text"> {">"} </span>
          <span className="light-th-body2 hint-text">
            {this.state?.productDetail?.product_type?.product_type_name}
          </span>
        </div>
        {this.state.product_title ? (
          <BidCardComponent
            product_id={this.id}
            title={this.state.product_title}
            price={this.state.current_price}
            endDate={this.state.end_date}
            bid={this.state.bid_price}
          />
        ) : (
          ""
        )}
        {this.state?.bidHis.length > 0 ? (
          <BidTableComponet hisData={this.state.bidHis}></BidTableComponet>
        ) : (
          ""
        )}
      </div>
    );
  }
}
const ProductDetailPageCom = (props) => (
  <ProductDetailPage {...props} params={useParams()} />
);
export default ProductDetailPageCom;
