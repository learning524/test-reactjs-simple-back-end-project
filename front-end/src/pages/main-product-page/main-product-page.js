import React from "react";
import "./main-product-page.css";
import ProductComponent from "../../components/product/product";
import { connect } from "react-redux";
import { getProduct } from "../../store/actions/prduct-actions";

class ProductPage extends React.Component {
  constructor() {
    super();
    this.lstProduct = [];
  }
  componentDidMount() {
    this.props.getProduct();
  }
  render() {
    if (this.props.products.lst.length > 0) {
      this.props.products.lst.forEach((element) => {
        this.lstProduct.push(
          <ProductComponent
            id={element.product_id}
            title={element.product_title}
            price={element.current_price}
            endDate={element.end_date}
            key={element.product_id}
          ></ProductComponent>
        );
      });
    }
    return (
      <div className="product-page-container ">
        <span className="header-title">สินค้าประมูล ราคาตามใจคุณ</span>
        {this.lstProduct.length > 0 ? this.lstProduct : ""}
      </div>
    );
  }
}

// export default ProductPage;

const mapStateToProps = (state) => {
  return { products: state.products };
};

export default connect(mapStateToProps, { getProduct })(ProductPage);
