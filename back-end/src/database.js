import { MongoMemoryServer } from "mongodb-memory-server";
import { MongoClient } from "mongodb";
import fs from "fs";
import path from "path";
let database = null;

export async function startDatabase() {
  // console.log(MongoMemoryServer);
  const mongo = new MongoMemoryServer();
  await mongo.start();
  const mongoUri = mongo.getUri();
  // const mongoDBURL = await mongo.getConnectionString();
  const connection = await MongoClient.connect(mongoUri, {
    useNewUrlParser: true,
  });

  // console.log("hello");
  if (!database) {
    // console.log("get DB");
    database = connection.db();
    const __dirname = path.resolve();
    // console.log(__dirname);
    const product = JSON.parse(
      fs.readFileSync(__dirname + "/source/mock_product.json")
    );
    const productHis = JSON.parse(
      fs.readFileSync(__dirname + "/source/mock_product_his.json")
    );
    // const productHis = JSON.parse(
    //   fs.readFileSync(__dirname + "/source/mock_product_his copy.json")
    // );
    const user = JSON.parse(
      fs.readFileSync(__dirname + "/source/mock_user.json")
    );
    const productType = JSON.parse(
      fs.readFileSync(__dirname + "/source/mock_product_type.json")
    );

    // const newProdHis = [];
    // let realIndex = 0;
    // product.forEach((prod, prodIndex) => {
    //   // console.log(newProdHis);
    //   let newGroup = false;
    //   productHis.forEach((prodHis, index) => {
    //     // console.log(prodHis);
    //     let newProdHisObj = { ...prodHis };
    //     // console.log(newProdHisObj);
    //     newProdHisObj.prodId = prod.product_id;
    //     if (newGroup == false) {
    //       newProdHisObj.bid = prod.current_price;
    //       const curDate = new Date();
    //       newProdHisObj.updateDate = +curDate;
    //     } else {
    //       newProdHisObj.bid = newProdHis[realIndex - 1].bid - prod.bid_price;
    //       const date = new Date();
    //       date.setDate(date.getDate() - index);
    //       newProdHisObj.updateDate = +date;
    //     }
    //     newProdHis.push(newProdHisObj);
    //     realIndex++;
    //     newGroup = true;
    //   });
    // });
    // // console.log(newProdHis);
    // console.log(JSON.stringify(newProdHis));
    await database.collection("product").insertMany(product);
    await database.collection("productHis").insertMany(productHis);
    await database.collection("user").insertMany(user);
    await database.collection("productType").insertMany(productType);
    // console.log(await database.collection("product").find().toArray());
  }

  return database;
}
