// const startDatabase = require("./database");
// const express = require("express");
import { startDatabase } from "./database.js";
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
const app = express();
let db = undefined;
let jsonParser = bodyParser.json();
let corsOptions = {
  origin: "http://localhost:3000",
};
// const loDb = db();
app.use(jsonParser);
app.use(cors(corsOptions));
app.get("/init", async (req, res) => {
  const lstProduct = await db
    .collection("product")
    .find({}, { projection: { _id: 0 } })
    .toArray();
  res.send({ lst: lstProduct });
});
app.post("/login", async (req, res) => {
  const criteria = req.body;
  console.log(criteria);
  const lstUser = await db
    .collection("user")
    .find({ username: criteria.username, password: criteria.password })
    .toArray();

  if (lstUser.length == 1) {
    res.send({ isAuth: true, username: lstUser[0].username });
  } else {
    return res.sendStatus(401);
  }
});

app.get("/productdetail", async (req, res) => {
  const productId = parseInt(req.query.prodid);
  if (db === undefined) {
    return res.sendStatus(500);
  }
  const productDetail = await db
    .collection("product")
    .aggregate([
      {
        $match: {
          product_id: productId,
        },
      },

      {
        $lookup: {
          from: "productType",
          localField: "product_type",
          foreignField: "product_type_id",
          as: "product_type",
        },
      },
      { $unwind: "$product_type" },
      {
        $project: {
          _id: 0,
          product_type: {
            _id: 0,
            product_type_id: 0,
          },
        },
      },
    ])
    .toArray();
  const productBidHis = await db
    .collection("productHis")
    .aggregate([
      {
        $match: {
          prodId: productId,
        },
      },
      {
        $unwind: "$update_by",
      },
      {
        $lookup: {
          from: "user",
          localField: "update_by",
          foreignField: "user_id",
          as: "user",
        },
      },
      { $unwind: "$user" },
      {
        $project: {
          _id: 0,
          update_by: 0,
          prodId: 0,
          user: {
            _id: 0,
            password: 0,
            user_id: 0,
          },
        },
      },
    ])
    .sort({ updateDate: -1 })
    .toArray();
  res.send({
    ...productDetail[0],
    bidHis: productBidHis,
  });
});

app.post("/updateBid", async (req, res) => {
  const currentDate = new Date();
  const criteria = req.body;
  // console.log(criteria);

  const product = await db
    .collection("product")
    .find({ product_id: criteria.product_id }, { projection: { _id: 0 } })
    .toArray();
  console.log(product[0].current_price, criteria.bid);
  if (parseInt(product[0].end_date) < Date.now()) {
    return res.status(406).send({ msg: "หมดเวลาการประมูล" });
  }
  if (product[0].current_price >= criteria.bid) {
    return res.status(406).send({ msg: "มีคนอื่นประมูลราคาสูงกว่าคุณแล้ว" });
  }
  await db.collection("product").update(
    { product_id: criteria.product_id },

    {
      $set: {
        current_price: criteria.bid,
      },
    }
  );

  await db.collection("productHis").update(
    { prodId: criteria.product_id, updateDate: +currentDate },
    {
      $set: {
        update_by: criteria.updateUser,
        prodId: criteria.product_id,
        bid: criteria.bid,
        updateDate: +currentDate,
      },
    },
    { upsert: true }
  );
  const lstProduct = await db
    .collection("product")
    .find({ product_id: criteria.product_id }, { projection: { _id: 0 } })
    .toArray();

  const lstProductHis = await db
    .collection("productHis")
    .find({ prodId: criteria.product_id }, { projection: { _id: 0 } })
    .toArray();
  // console.log(lstProductHis);
  res.send(req.body);
});

// async function getAllProduct() {
//   return;
// }

const port = 4100;
// app.use(bodyParser.json());
app.listen(port, async () => {
  // console.log(db);
  db = await startDatabase();
  console.log(`server start port :: ${port}`);
});
